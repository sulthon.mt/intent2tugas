package com.example.intent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class activity3 extends AppCompatActivity implements View.OnClickListener {
    Button b1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity3);
        b1 = findViewById(R.id.b1);
        b1.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        Intent explicit = new Intent(activity3.this, activity4.class);
        startActivity(explicit);
    }


}
